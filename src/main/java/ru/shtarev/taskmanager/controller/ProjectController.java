package ru.shtarev.taskmanager.controller;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.shtarev.taskmanager.entity.Project;
import ru.shtarev.taskmanager.service.ProjectService;

import java.util.List;

@RestController
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @PostMapping(value = "/projects")
    public ResponseEntity<Project> addNewProject(@RequestBody @NotNull final Project project) {
        return ResponseEntity.ok(projectService.saveProject(project).get());
    }


    @GetMapping(path = "/projects/{id}")
    public ResponseEntity<Project> getProject(@PathVariable(value = "id") final String id) {
        return ResponseEntity.of(projectService.getProject(id));
    }

    @GetMapping(path = "/projects")
    public ResponseEntity<List<Project>> getAllProject(
            @PageableDefault(sort = "id") Pageable pageable,
            @RequestParam(name = "userName") final String userName) {
        return ResponseEntity.ok(projectService.getAllProjects(pageable, userName));
    }


    @PutMapping(path = "/projects")
    public ResponseEntity<Project> updateProject(@RequestBody @NotNull final Project newProject) {
        return ResponseEntity.ok(projectService.updateProject(newProject).get());
    }


    @DeleteMapping(path = "/projects/{id}")
    public ResponseEntity<Project> deleteProject(@PathVariable(value = "id") final String id) {
        projectService.deleteProject(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/projects/{userId}")
    public ResponseEntity<Project> deleteProjects(@PathVariable(value = "userId") final String userId) {
        projectService.deleteProjects(userId);
        return ResponseEntity.ok().build();
    }

}
