package ru.shtarev.taskmanager.controller;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.shtarev.taskmanager.entity.Task;
import ru.shtarev.taskmanager.service.TaskService;

import java.util.List;

@RestController
public class TaskController {

    private final  TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(value = "/tasks")
    public ResponseEntity<Task> addNewTask(@RequestBody @NotNull final Task task) {
        return ResponseEntity.ok(taskService.saveTask(task));
    }


    @GetMapping(path = "/tasks/{id}")
    public ResponseEntity<Task> getTask(@PathVariable(value = "id") final String id) {
        return ResponseEntity.ok(taskService.getTask(id));
    }

    @GetMapping(path = "/tasks")
    public ResponseEntity<List<Task>> getAllTasks(@PageableDefault Pageable pageable,
                                                  @RequestParam(name = "projectId") final String projectId) {
        return ResponseEntity.ok(taskService.getAllTasks(pageable, projectId));
    }


    @PutMapping(path = "/tasks")
    public ResponseEntity<Task> updateTask(@RequestBody @NotNull final Task newTask) {
        return ResponseEntity.ok(taskService.updateTask(newTask).get());
    }


    @DeleteMapping(path = "/tasks/{id}")
    public ResponseEntity<Object> deleteTask(@PathVariable(value = "id") final String id) {
        taskService.deleteTask(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/tasks/{projectId}")
    public ResponseEntity<Task> deleteTasksByProjectId(@PathVariable(value = "projectId") final String projectId) {
        taskService.deleteTasksByProjectId(projectId);
        return ResponseEntity.ok().build();
    }


    @DeleteMapping(path = "/tasks/{userId}")
    public ResponseEntity<Task> deleteTasks(@PathVariable(value = "userId") final String userId) {
        taskService.deleteTasks(userId);
        return ResponseEntity.ok().build();
    }

}
