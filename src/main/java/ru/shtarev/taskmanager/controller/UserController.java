package ru.shtarev.taskmanager.controller;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.shtarev.taskmanager.entity.User;
import ru.shtarev.taskmanager.entity.Users;
import ru.shtarev.taskmanager.service.UserService;

import java.util.List;

@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/users")
    public ResponseEntity<User> addNewUser(@RequestBody @NotNull final User user) {
        return ResponseEntity.of(userService.saveUser(user));
    }


    @GetMapping(path = "/users/{name}")
    public ResponseEntity<User> getUser(@PathVariable(value = "name") final String name) {
        return ResponseEntity.of(userService.getUserByName(name));
    }

    @GetMapping(path = "/users")
    public ResponseEntity<Users> getAllUser(@PageableDefault(sort = "name", size = 10) Pageable pageable) {
        return ResponseEntity.of(userService.getAllUsers(pageable));
    }

    @PutMapping(path = "/users")
    public ResponseEntity<User> updateUser(@RequestBody @NotNull final User newUser) {
        return ResponseEntity.of(userService.update(newUser));
    }

    @DeleteMapping(path = "/users/{userName}")
    public ResponseEntity<User> deleteUser(@PathVariable String userName) {
        userService.deleteByName(userName);
        return ResponseEntity.ok().build();
    }
}
