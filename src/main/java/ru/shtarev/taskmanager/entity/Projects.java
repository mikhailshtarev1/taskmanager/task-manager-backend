package ru.shtarev.taskmanager.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class Projects {
    private List<Project> listProject;
}

