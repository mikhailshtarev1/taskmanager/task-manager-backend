package ru.shtarev.taskmanager.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;
import ru.shtarev.taskmanager.enumpackage.TaskProjectStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "tasks")
@Validated
public class Task {

    @Id
    @NotNull
    @Column(name = "task_id", nullable = false)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "task_name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "task_description", nullable = false)
    private String description;

    @NotNull
    @Past
    @Column(name = "data_start", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime dataStart;

    @NotNull
    @Past
    @Column(name = "data_finish", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime dataFinish;


    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    private Project project;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;

    @Column(name = "task_project_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TaskProjectStatus taskProjectStatus;

    @PastOrPresent
    @Column(name = "data_create", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime dataCreate;

}