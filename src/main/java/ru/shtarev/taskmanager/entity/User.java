package ru.shtarev.taskmanager.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;
import ru.shtarev.taskmanager.enumpackage.UserRole;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.UUID;


@Entity
@Getter
@Setter
@Table(name = "users")
@Validated
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User {
    @NotNull
    @Size(min=6, max=255)
    @Column(name = "user_name", unique = true, nullable = false)
    private String name;

    @NotNull
    @Size(min=6, max=255)
    @Column(name = "user_password", nullable = false, length = 100)
    private String password;


    @ElementCollection(targetClass = UserRole.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_id_user_role", joinColumns =
    @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    @Column(name = "user_role", nullable = false)
    private Set<UserRole> userRole;

    @Id
    @Column(name = "user_id", unique = true, nullable = false)
    private String userId;

}

