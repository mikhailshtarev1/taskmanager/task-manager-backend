package ru.shtarev.taskmanager.enumpackage;

public enum TaskProjectStatus {
    PLANNED, IN_THE_PROCESS, READY
}

