package ru.shtarev.taskmanager.enumpackage;

public enum UserRole {
    ADMIN,
    REGULAR_USER,
    PREMIUM_USER,
    NO_ROLE

}
