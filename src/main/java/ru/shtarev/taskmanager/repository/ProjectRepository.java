package ru.shtarev.taskmanager.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.shtarev.taskmanager.entity.Project;
import ru.shtarev.taskmanager.entity.User;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    List<Project> findAllByUser( Pageable pageable, User user);


    void deleteAllByUser (String userId);
}
