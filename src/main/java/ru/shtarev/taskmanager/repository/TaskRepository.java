package ru.shtarev.taskmanager.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.shtarev.taskmanager.entity.Project;
import ru.shtarev.taskmanager.entity.Task;


import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    List<Task> findAllByProject(Pageable pageable, Project project);

    void deleteByProject(String projectId);

    void deleteAllByUser(String userId);
}
