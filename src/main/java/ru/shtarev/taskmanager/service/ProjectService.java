package ru.shtarev.taskmanager.service;

import org.springframework.data.domain.Pageable;
import ru.shtarev.taskmanager.entity.Project;
import ru.shtarev.taskmanager.entity.Projects;

import java.util.List;
import java.util.Optional;

public interface ProjectService {

    Optional<Project> saveProject(Project project);


    Optional<Project> getProject(String id);

    List<Project> getAllProjects(Pageable pageable, String userId);


    Optional<Project> updateProject(Project newProject);


    boolean deleteProject(String id);

    boolean deleteProjects(String userId);
}
