package ru.shtarev.taskmanager.service;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.shtarev.taskmanager.entity.Project;
import ru.shtarev.taskmanager.entity.User;
import ru.shtarev.taskmanager.repository.ProjectRepository;
import ru.shtarev.taskmanager.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final UserRepository userRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, UserRepository userRepository) {
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Optional<Project> saveProject(Project project) {
        project.setDataCreate(LocalDateTime.now());
        return Optional.of(projectRepository.save(project));
    }

    @Override
    public Optional<Project> getProject(String id) {
        return projectRepository.findById(id);
    }

    @Override
    public List<Project> getAllProjects(Pageable pageable, String userName) {
        User user = userRepository.findByName(userName).get();
        return projectRepository
                .findAllByUser(pageable,user);
    }

    @Override
    public Optional<Project> updateProject(Project newProject) {
        @NotNull final Optional<Project> projectOpt = projectRepository.findById(newProject.getId());
        if (!projectOpt.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(projectRepository.save(newProject));
    }

    @Override
    public boolean deleteProject(String id) {
        projectRepository.deleteById(id);
        return true;
    }

    @Override
    public boolean deleteProjects(String userId) {
        projectRepository.deleteAllByUser(userId);
        return true;
    }
}
