package ru.shtarev.taskmanager.service;

import org.springframework.data.domain.Pageable;
import ru.shtarev.taskmanager.entity.Task;

import java.util.List;
import java.util.Optional;

public interface TaskService {
    Task saveTask(Task task);


   Task getTask(String id);

    List<Task> getAllTasks(Pageable pageable, String projectId);


    Optional<Task> updateTask(Task newTask);


    boolean deleteTask(String id);

    boolean deleteTasksByProjectId(String projectId);


    boolean deleteTasks(String userId);
}
