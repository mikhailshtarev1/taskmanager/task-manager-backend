package ru.shtarev.taskmanager.service;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.shtarev.taskmanager.entity.Project;
import ru.shtarev.taskmanager.entity.Task;
import ru.shtarev.taskmanager.repository.ProjectRepository;
import ru.shtarev.taskmanager.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;

    @Autowired
    public TaskServiceImpl(TaskRepository taskRepository, ProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }




    @Override
    public Task saveTask(Task task) {
        return taskRepository.save(task);
    }

    @Override
    public Task getTask(String id) {
        return taskRepository.findById(id).get();
    }

    @Override
    public List<Task> getAllTasks(Pageable pageable,String projectId) {
        Project project = projectRepository.findById(projectId).get();
        return taskRepository.findAllByProject( pageable,project);

    }

    @Override
    public Optional<Task> updateTask(Task newTask) {
        @NotNull final Optional<Task> taskOpt
                = taskRepository.findById(newTask.getId());
        if (!taskOpt.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(taskRepository.save(newTask));
    }

    @Override
    public boolean deleteTask(String id) {
        taskRepository.deleteById(id);
        return true;
    }

    @Override
    public boolean deleteTasksByProjectId(String projectId) {
        taskRepository.deleteByProject(projectId);
        return true;
    }

    @Override
    public boolean deleteTasks(String userId) {
        taskRepository.deleteAllByUser(userId);
        return true;
    }
}
