package ru.shtarev.taskmanager.service;

import org.springframework.data.domain.Pageable;
import ru.shtarev.taskmanager.entity.User;
import ru.shtarev.taskmanager.entity.Users;

import java.util.List;
import java.util.Optional;


public interface UserService {

    Optional<User> saveUser(User user);


    Optional<User> getUserByName(String name);

    Optional<Users> getAllUsers(Pageable pageable);


    Optional<User> update(User newUser);


    boolean deleteByName(String userName);
}
