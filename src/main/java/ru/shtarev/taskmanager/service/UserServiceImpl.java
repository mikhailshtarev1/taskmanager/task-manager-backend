package ru.shtarev.taskmanager.service;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.shtarev.taskmanager.entity.User;
import ru.shtarev.taskmanager.entity.Users;
import ru.shtarev.taskmanager.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Optional<User> saveUser(@NotNull final User newUser) {
        return Optional.of(userRepository.save(newUser));

    }

    @Override
    public Optional<User> getUserByName(@NotNull final String name) {
        if (name.isEmpty()) {
            throw new NullPointerException();
        }
        Optional<User> userOpt = userRepository.findByName(name);
//        if(!userOpt.isPresent())
//            throw new NullPointerException();
//        if (!Md5.md5(password).equals(userOpt.get().getPassword()))
//            throw new NullPointerException();
        return userOpt;
    }

    @Override
    public Optional<Users> getAllUsers(Pageable pageable) {
        Page<User> pageResult = userRepository.findAll(pageable);
        List<User> listuser = pageResult.getContent();
        Users users = new Users();
        users.setListUser(listuser);
        return Optional.of(users);
    }


    @Override
    public Optional<User> update(User newUser) {
        @NotNull final Optional<User> userOpt = userRepository.findByName(newUser.getName());
        if (!userOpt.isPresent()) {
            return Optional.empty();
        }
        User user = userOpt.get();
        user.setUserRole(newUser.getUserRole());
        user.setPassword(newUser.getPassword());
        return Optional.of(userRepository.save(user));
    }


    @Override
    public boolean deleteByName(String userName) {
        userRepository.deleteByName(userName);
        return true;
    }
}
